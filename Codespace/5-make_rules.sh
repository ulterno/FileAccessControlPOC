#!/bin/bash
# To be done by controller

## Make rules.json
cd intermeddiate_files
../programs/jsonmaker
cd ..

## Sign rules.json by Controller
gpg --homedir gpg_dirs/controller --batch --yes --pinentry-mode loopback --passphrase controllerpass --sign-with Controller --output intermeddiate_files/rules.json.signed --sign intermeddiate_files/rules.json

### (C) = rules.json.signed ##
