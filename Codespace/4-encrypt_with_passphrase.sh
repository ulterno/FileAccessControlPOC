#!/bin/bash
# To be done by controller

## Encrypt file with passphrase
gpg --homedir gpg_dirs/controller --batch --yes --passphrase-file example_passphrase --recipient Author --output intermeddiate_files/indatafile.txt.signed.encrypted --symmetric --cipher-algo aes256 --encrypt intermeddiate_files/indatafile.txt.signed

### (A) = indatafile.txt.signed.encrypted ##

## Encrypt passphrase to be given to Server
gpg --homedir gpg_dirs/controller --batch --yes --recipient Server --output intermeddiate_files/passphrase.gpg --encrypt example_passphrase

### (B) = passphrase.gpg ##
