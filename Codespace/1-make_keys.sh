#!/bin/bash
mkdir -p gpg_dirs/author gpg_dirs/controller gpg_dirs/server
mkdir intermeddiate_files

# Make keys
### Have to use normal key generation for now
gpg --homedir gpg_dirs/author --batch --gen-key author_keygen_info
gpg --homedir gpg_dirs/controller --batch --gen-key controller_keygen_info
gpg --homedir gpg_dirs/server --batch --gen-key server_keygen_info
# echo "============== Author author@org ====================="
# gpg --homedir gpg_dirs/author --pinentry-mode loopback --passphrase authorpass --yes --gen-key
# echo "============== Controller controller@org ====================="
# gpg --homedir gpg_dirs/controller --pinentry-mode loopback --passphrase controllerpass --yes --gen-key
# echo "============== Server server@org ====================="
# gpg --homedir gpg_dirs/server --pinentry-mode loopback --passphrase serverpass --yes --gen-key

# Export public keys
mkdir exported_keys
gpg --homedir gpg_dirs/author --export > exported_keys/Author_key_export
gpg --homedir gpg_dirs/author --export-ownertrust > exported_keys/Author_key_export_t
gpg --homedir gpg_dirs/controller --export > exported_keys/Controller_key_export
gpg --homedir gpg_dirs/controller --export-ownertrust > exported_keys/Controller_key_export_t
gpg --homedir gpg_dirs/server --export > exported_keys/Server_key_export
gpg --homedir gpg_dirs/server --export-ownertrust > exported_keys/Server_key_export_t

# Import other's keys
gpg --homedir gpg_dirs/author --import exported_keys/Controller_key_export exported_keys/Server_key_export
gpg --homedir gpg_dirs/controller --import exported_keys/Author_key_export exported_keys/Server_key_export
gpg --homedir gpg_dirs/server --import exported_keys/Author_key_export exported_keys/Controller_key_export
gpg --homedir gpg_dirs/author --import-ownertrust exported_keys/Controller_key_export_t
gpg --homedir gpg_dirs/author --import-ownertrust exported_keys/Server_key_export_t
gpg --homedir gpg_dirs/controller --import-ownertrust exported_keys/Author_key_export_t
gpg --homedir gpg_dirs/controller --import-ownertrust exported_keys/Server_key_export_t
gpg --homedir gpg_dirs/server --import-ownertrust exported_keys/Author_key_export_t
gpg --homedir gpg_dirs/server --import-ownertrust exported_keys/Controller_key_export_t
