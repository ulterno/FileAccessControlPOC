#!/bin/bash
# Make user workspace
mkdir gpg_dirs/user
gpg --homedir gpg_dirs/user --import exported_keys/Author_key_export
gpg --homedir gpg_dirs/user --import-ownertrust exported_keys/Author_key_export_t

mkdir decryption_intermeddiate_files
cp FinalFile.secure decryption_intermeddiate_files/
cd decryption_intermeddiate_files

# User will untar archive
tar -xf FinalFile.secure

# Read (C) -> rules.json (Both user and server will do this)
gpg --homedir ../gpg_dirs/user --batch --yes --output rules.json --decrypt crypt/rules.json.signed
# gpg --homedir ../gpg_dirs/server --batch --yes --output rules.json --decrypt crypt/rules.json.signed
../programs/jsonextractor

## Decrypt passphrase (Only server may do this.)
## The server is supposed to do this after making sure that user has access according to policy
gpg --homedir ../gpg_dirs/server --batch --yes --pinentry-mode loopback --passphrase serverpass --output passphrase_out --decrypt passphrase_out.gpg

# The server can now send the decrypted passphrase_out to the user

# User will decrypt main file (A)
gpg --homedir ../gpg_dirs/user --batch --yes --passphrase-file passphrase_out --output indatafile.txt.signed --decrypt crypt/indatafile.txt.signed.encrypted

# Optional: If Author has signed the file before encryption, user will extract the file out of the signed file using Author Public Key
gpg --homedir ../gpg_dirs/user --batch --yes --output ../indatafile_out.txt --decrypt indatafile.txt.signed

