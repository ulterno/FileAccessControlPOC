# File Access Control POC

## How to use
0.  Build jsonmaker and jsonextractor
1.  Make a directory named Workspace
2.  Copy the files:
    1.  "example_passphrase" to Workspace
    2.  "jsonmaker" executable to Workspace/programs
    2.  "jsonextractor" executable to Workspace/programs
    3.  "author_keygen_info", "controller_keygen_info" and "server_keygen_info" to Workspace
3.  Go into directory "Workspace"
4.  Run shell scripts from Codespace (that start with numbers) in numerical order

## Concept
-   We have multiple entities in the system, each having an identity (a GPG key).
    -   All other entities are assumed to have whatever public keys they require.
    
    1.  Author: creates the secret document (and optionally signs it) [Author-SK, Author-PK]
    2.  Controller: encrypts the file, along with metadata required to decide who opens it. [Controller-SK, Controller-PK]
    3.  Server: provides the metadata-policy checking and decryption service. [Server-SK, Server-PK]
    4.  User: receives the encrypted file, asks the server for the decryption key
        -   optionally has its own GPG key for user authentication and secure transmission of decrypted key. [User-SK, User-PK]

### Process

1.  We start with the Author Signing the Secret File (W) using the Author-SK, to get a Signed Secret File (X)

    ~~~plantuml
    [*] -> W
    W : Original Unencrypted File
    W --> X : Signed by Author-SK
    X : Signed File, Considered Unencrypted
    ~~~

2.  User then gives the signed file (X) to Controller.
3.  Controller
    1.  Generates Passphrase (G)
        -   possibly randomly
        -   is a large string
    2.  Encrypts Signed File (X) using the passphrase (G)
        -   Results in encrypted file (A)
    3.  Encrypts Passphrase (G) with Server-PK
        -   Results in encrypted file (B)
            -   can only be opened using Server-SK (which is to only be available to the Server entity)
    4.  Creates a file, rules.json (H_0)
        1.  Creates metadata for (X) and adds to (H_0), creating (H_1)
        2.  Adds (B) to (H_1), finally (H)
    5.  Signs (H) using Controller-SK
        -   Resulting in a Signed Metadata file (C)
    6.  Combines (A) and (C) into a new file (Y)
        
    ~~~plantuml
    X : Signed File from Author
    [*] --> G : Random Generation
    G : Large Passphrase
    G : Key for Symmetric Encryption
    X --> A : Using G \n(Large Passphrase)
    A : Encrypted Secret file
    A : Using Symmetric Encryption
    G --> B : Encrypt with Server-PK
    B : Encrypted Passphrase
    B : Can only be decrypted by Server
    --
    [*] -> H_0 : Create File
    H_0 : Empty Metadata file (rules.json)
    H_0 --> H_1 : Add metadata for X
    H_1 --> H : Add B \n(Encrypted Passphrase)
    H : Unencrypted Metadata file
    H : Contains Encryped Passphrase
    H --> C : Sign with Controller-SK
    C : Signed Metadata file
    C : Makes sure any tampering will invalidate file
    --
    [*] --> Y : Create File \n add A and C
    Y : Final Encrypted File
    Y : Ready to send
    Y --> [*] : Send to User
    ~~~

4.  User
    1.  Receives (Y)
    2.  Extracts (A) and (C) from (Y)
    3.  Sends (C) to Server, along with authenticating itself to server
5.  Server
    1.  Verifies User authentication
    2.  Receives (C)
    3.  Verifies signature on (C)
    4.  Reads metadata and according to policy, determines whether or not, user gets access to the Passphrase
        -   If not, then Server gives error and exits
    5.  Extracts (B) from (C)
    6.  Decrypts Passphrase (B), obtaining (G)
    7.  Opens a secure channel with User
    8.  Sends Passphrase (G)
6.  User
    1.  Receives Passphrase (G)
    2.  Decrypts (A) to obtain (X)
    3.  Securely Discards (G)
    4.  Verifies (X) with Author-PK and extract (W)
    5.  User now has the original file

    ~~~plantuml
    [*] --> Y : Receive
    Y --> A : Extract \n No key required
    Y --> C : Extract \n No key required
    A : Encrypted Secret File
    C : Signed
    C : Contains metadata and Encrypted Passphrase C
    C --> [*] : Send to Server
    ~~~
    ~~~plantuml
    [*] --> C : Receive from \n Authenticated User
    C : Signed by Controller
    C --> H : Verify and Extract with \n Controller-PK
    H : Metadata File
    H --> B : Extract
    H --> Metadata : Extract
    Metadata : Used to Determine access by checking policy
    B : Encrypted Passphrase
    B : Can be decrypted only by Server
    B --> G : Decrypt with Server-SK
    G : Passphrase for Symmetric Encryption of Secret File
    G --> [*] : Send to user
    ~~~
    ~~~plantuml
    [*] --> G : Receive from Server
    G : Used to Decrypt A
    A --> X : Decrypt using A
    X : Signed Secret file from Author
    G --> [*] : Discard Passphrase
    X --> W : Verify and Extract with \n Author-PK
    W : The file User asked for
    ~~~
        
Alternatively, Encrypted Passphrase can be sent to the server instead and not added to (C).
This is useful in case you consider the Available Public Key Encryption and Signing to be inadequate for long term purposes.

[GPG Concepts Used](GPG_Concepts_Used.md)
