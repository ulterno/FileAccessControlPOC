# GPG Concepts Used

## Public Key Encryption
-   Entity "P" creates a Private Key/Secret Key (SK) for a Public-key cipher function and creates a Public Key (PK) that is sent to other entities "Q".
-   "Q" can encrypt a file using the PK provided by "P", such that it can only be decrypted by "P".

[Reference](https://www.gnupg.org/gph/en/manual/x195.html)

## Digital Signature
-   Using the same setup as in Public Key Encryption
-   "P" can sign a file using its SK, such that "Q", having the corresponding PK can verifiy that the file was signed by "P".

[Reference](https://www.gnupg.org/gph/en/manual/x215.html)

## Symmetric Encryption
-   Encrypt a file with a Key.
-   Decrypt the encrypted file somewhere else with the same key.

[Reference](https://www.gnupg.org/gph/en/manual/c173.html)
