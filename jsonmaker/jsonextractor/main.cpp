#include <QJsonDocument>
#include <QJsonObject>
#include <QByteArray>
#include <QString>
#include <QFile>

int main(int argc, char *argv[])
{
	QFile rulesFile ("rules.json");
	if (!rulesFile.open(QFile::ReadOnly))
	{
		return -20;
	}
	QByteArray rulesData = rulesFile.readAll();
	rulesFile.close();
	QJsonParseError err;
	QJsonDocument rulesDoc (QJsonDocument::fromJson(rulesData, &err));
	if (err.error != QJsonParseError::NoError)
	{
		return -5;
	}
	QByteArray ppgpgdata = QByteArray::fromBase64(rulesDoc.object()["password"].toString().toLatin1());

	QFile outFile ("passphrase_out.gpg");
	if (!outFile.open(QFile::WriteOnly))
	{
		return -10;
	}
	outFile.write(ppgpgdata);
	outFile.close();
	return 0;
}
