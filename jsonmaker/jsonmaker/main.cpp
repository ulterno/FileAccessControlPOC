#include <QJsonDocument>
#include <QJsonObject>
#include <QByteArray>
#include <QString>
#include <QFile>

int main(int argc, char *argv[])
{
	QFile inFile ("passphrase.gpg");
	if (!inFile.open(QFile::ReadOnly))
	{
		return -10;
	}
	QByteArray ppgpgdata = inFile.readAll();
	inFile.close();
	QJsonObject outObj;
	outObj.insert("access" , true);
	outObj.insert("password", QString::fromLatin1(ppgpgdata.toBase64()));


	QFile outFile ("rules.json");
	if (!outFile.open(QFile::WriteOnly))
	{
		return -20;
	}
	outFile.write(QJsonDocument(outObj).toJson());
	outFile.close();
	return 0;
}
